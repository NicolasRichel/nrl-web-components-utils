# Web Component utils

Useful utility functions to develop web components.

### Install

```
$ npm install @nrl/web-component-utils
```

### Development

 - **(1)** Clone this repository.

 - **(2)** Build: `npm run build:dev`

### Publish

```
$ npm version <major|minor|patch>
$ npm publish
```

# License

The content of this repository is published under the terms of the [GNU LGPL v3](./LICENSE.txt).
