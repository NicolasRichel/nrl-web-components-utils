export function createShadowDOM(component: HTMLElement, template: HTMLTemplateElement): DocumentFragment;

export function reflectAttributes(component: HTMLElement, attr: string | string[]): HTMLElement;

export function reflectBooleanAttribute(component: HTMLElement, attr: string): HTMLElement;

export function reflectNumberAttribute(component: HTMLElement, attr: string): HTMLElement;

export function reflectObjectAttribute(component: HTMLElement, attr: string): HTMLElement;

export function reflectStringAttribute(component: HTMLElement, attr: string): HTMLElement;
