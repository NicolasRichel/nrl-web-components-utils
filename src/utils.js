/**
 * @returns {DocumentFragment}
 */
 function createShadowDOM(component, template) {
  if (!(component instanceof HTMLElement)) {
    throw new TypeError("[web-components-utils] - Invalid argument: `component` must be an HTMLElement.");
  }
  if (!(template instanceof HTMLTemplateElement)) {
    throw new TypeError("[web-components-utils] - Invalid argument: `template` must be an HTMLTemplateElement.");
  }
  const shadowRoot = component.attachShadow({ mode: "open" });
  shadowRoot.appendChild( template.content.cloneNode(true) );
  return shadowRoot;
}

function reflectStringAttribute(component, attr) {
  if (!(component instanceof HTMLElement)) {
    throw new TypeError("[web-components-utils] - Invalid argument: `component` must be an HTMLElement.");
  }
  if (typeof attr !== "string") {
    throw new TypeError("[web-components-utils] - Invalid argument: `attr` must be a string.")
  }
  Object.defineProperty(component, attr, {
    get() {
      return component.getAttribute(attr);
    },
    set(value) {
      component.setAttribute(attr, value);
    }
  });
  return component;
}

function reflectNumberAttribute(component, attr) {
  if (!(component instanceof HTMLElement)) {
    throw new TypeError("[web-components-utils] - Invalid argument: `component` must be an HTMLElement.");
  }
  if (typeof attr !== "string") {
    throw new TypeError("[web-components-utils] - Invalid argument: `attr` must be a string.")
  }
  Object.defineProperty(component, attr, {
    get() {
      return Number(component.getAttribute(attr));
    },
    set(value) {
      component.setAttribute(attr, value);
    }
  });
  return component;
}

function reflectBooleanAttribute(component, attr) {
  if (!(component instanceof HTMLElement)) {
    throw new TypeError("[web-components-utils] - Invalid argument: `component` must be an HTMLElement.");
  }
  if (typeof attr !== "string") {
    throw new TypeError("[web-components-utils] - Invalid argument: `attr` must be a string.")
  }
  Object.defineProperty(component, attr, {
    get() {
      return component.hasAttribute(attr);
    },
    set(value) {
      if (value) {
        component.setAttribute(attr, "");
      } else {
        component.removeAttribute(attr);
      }
    }
  });
  return component;
}

function reflectObjectAttribute(component, attr) {
  if (!(component instanceof HTMLElement)) {
    throw new TypeError("[web-components-utils] - Invalid argument: `component` must be an HTMLElement.");
  }
  if (typeof attr !== "string") {
    throw new TypeError("[web-components-utils] - Invalid argument: `attr` must be a string.")
  }
  Object.defineProperty(component, attr, {
    get() {
      return JSON.parse(component.getAttribute(attr));
    },
    set(value) {
      component.setAttribute(attr, JSON.stringify(value));
    }
  });
  return component;
}

function reflectAttributes(component, attributes) {
  if (!(component instanceof HTMLElement)) {
    throw new TypeError("[web-components-utils] - Invalid argument: `component` must be an HTMLElement.");
  }
  attributes = [].concat(attributes);
  if (attributes.some(attr => typeof attr !== "string")) {
    throw new TypeError("[web-components-utils] - Invalid argument: `attributes` must be a string or an array of strings.");
  }
  for (const attr of attributes) {
    const [name, type] = attr.split(":");
    switch (type) {
      case "number":
        reflectNumberAttribute(component, name);
        break;
      case "boolean":
        reflectBooleanAttribute(component, name);
        break;
      case "object":
      case "array":
        reflectObjectAttribute(component, name);
        break;
      default:
        reflectStringAttribute(component, name);
        break;
    }
  }
  return component;
}

export {
  createShadowDOM,
  reflectAttributes,
  reflectBooleanAttribute,
  reflectNumberAttribute,
  reflectObjectAttribute,
  reflectStringAttribute
};
